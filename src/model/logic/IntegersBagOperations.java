package model.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public double add(IntegersBag bag)
	{
		int num = 0;
		int result=0;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				num=iter.next();
				result+=num;
			}
		}
		return result;
	}
	
	public double generateRandomNumber(IntegersBag bag)
	{
		int num = 0;
		ArrayList<Integer> list = new ArrayList<>();
		Iterator <Integer> iter = bag.getIterator();
		while(iter.hasNext())
		{
			list.add(iter.next());
		}
		Collections.shuffle(list);
		for(int i=0; i<list.size();i++)
		{
			num=list.get(1);
		}
		return num;
	}
	
	public double multiply (IntegersBag bag)
	{
		int num = 1;
		int result=1;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				num=iter.next();
				result*=num;
			}
		}
		return result;
	}
}
