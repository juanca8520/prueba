package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag){
		return model.getMax(bag);
	}


	public static double getSum(IntegersBag bag)
	{
		return model.add(bag);
		// TODO Auto-generated method stub
		
	}


	public static double getRandom(IntegersBag bag)
	{
		return model.generateRandomNumber(bag);
		// TODO Auto-generated method stub
	}


	public static double getProduct(IntegersBag bag) 
	{
		return model.multiply(bag);
		// TODO Auto-generated method stub
		
	}
	
	
}
